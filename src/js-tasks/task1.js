const processor1 = (v) => v * v;
const processor2 = (v, w) => v * w * 2;

const aProcessor1 = (v, cb) => cb(v * v);
const aProcessor2 = (v, w, cb) => cb(v * w * 2);

// sync
const calculate = (input) => {
  const v = processor1(input);
  return processor2(input, v);
};

// async
const asyncCalculate = (input, cb) => {
  aProcessor1(input, (res) => {
    aProcessor2(input, res, cb);
  });
};

