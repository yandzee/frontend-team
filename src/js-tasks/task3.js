function calculate(input, cb) {
  const resolver = function resolver(qux) {
    if (qux >= input) {
      return cb(qux);
    }

    boggle(input, qux, function(v) {
      resolver(qux + v)
    });
  };

  fizzle(input, resolver);
}

const processor1 = (v) => v * v;
const processor2 = (v, w) => v * w * 2;

const aProcessor1 = (v, cb) => cb(v * v);
const aProcessor2 = (v, w, cb) => cb(v * w * 2);

// sync 1
const calculate = (input) => {
  const v = processor1(input);

  while (input < v) {
    v = v + processor2(input, v);
  }

  return v;
};

// async 1
const asyncCalculate = (input, cb) => {
  const resolver = (v) => {
    if (input >= v) {
      return cb(v);
    }

    aProcessor2(input, v, (res) => {
      resolver(v + res);
    });
  };

  aProcessor1(input, resolver);
};

