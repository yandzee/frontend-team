const processor1 = (v) => v * v;
const processor2 = (v, w) => v * w * 2;

const aProcessor1 = (v, cb) => cb(v * v);
const aProcessor2 = (v, w, cb) => cb(v * w * 2);

// sync
const calculate = (input) => {
  const v = processor1(input);

  if (input < v) {
    v = v + processor2(input, v);
  }

  return v;
};

// async
const asyncCalculate = (input, cb) => {
  aProcessor1(input, (v) => {
    if (v >= input) {
      return cb(v);
    }

    aProcessor2(input, v, (res) => {
      cb(v + res);
    });
  });
};

