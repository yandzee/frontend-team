function mul(a, b) {
  return a * b;
}

function run() {
  let a = mul(5, 2);

  console.log(a);
}

run();
